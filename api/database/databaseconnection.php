<?php
/*
*
* @project		:	PROJECT NAME
*
* @description	:	GENERAL PROJECT DESRCIPTION
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	DATE OF COMMENT
*
* @kick-off		:	PROJECT START DATE
*
* @launch		:	PROJECT LAUNCH DATE
*
*
*
* @content		:	Index web page
*
* @remarks		:
*
* @change log	:
*
*/

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "api/physical/database.php";
require_once "app/core/server.php";

use App\Core\Error\ErrorMessage;
use App\Core\Error\Errorhandling;

define("MY_DOMAIN_SERVER", "project.vandartel-kniknie.nl");

/* DATABASE CONNECTION */

if($_SERVER["SERVER_NAME"] === MY_DOMAIN_SERVER) {
	$GLOBALS["hostname"] = "localhost";
	$GLOBALS["username"] = "project_api";
	$GLOBALS["password"] = "FfwLz?J^xvQMEdC7#gvgwwTzAtS6MH";
	$GLOBALS["db_name"] = "project";
} elseif($_SESSION["local"]) {
	$GLOBALS["hostname"] = "localhost";
	$GLOBALS["username"] = "project_api";
	$GLOBALS["password"] = "FfwLz?J^xvQMEdC7#gvgwwTzAtS6MH";
	$GLOBALS["db_name"] = "project";
}

$DBerror = "No database connection: A database is required.";
$GLOBALS["DBerrorMessage"] = new ErrorMessage(ErrorMessage::APP_SYSTEMERROR, ErrorMessage::ERROR_ERRORTYPE, 1, $DBerror, debug_backtrace());

if(empty($GLOBALS["db_name"]) || !preg_match("/^project$/", $GLOBALS["db_name"])) {
	$ErrorHandling = new ErrorHandling(new File("shared/logs", "error.log"), !$_SESSION["local"]);
	$ErrorHandling->AddMessage($DBerrorMessage);
	$ErrorHandling->KillSystem();
}
?>
