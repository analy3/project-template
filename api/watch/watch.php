<?php

namespace Debug;

class WatchItem
{
    /* string $label */
    private $label;

    /* mixed $item2watch */
    private $item2watch;

    public function __construct(string $label, $item2watch)
    {
        $this->label = ($label[0] == "\$" ? "" : "\$").$label;
        ob_start();
        var_dump($item2watch);
        $this->item2watch = ob_get_clean();
    }

    public function getItem2Watch(): array
    {
        return array($this->label => $this->item2watch);
    }
};

class Watch
{
    const WATCH_JSON = "/project/api/json/watch.json";

    /* array $WatchItems */
    private $WatchItems;

    /* int $nitems */
    private $nitems;

    /* array watchList */
    private $watchList;

    public function __construct()
    {
        $this->WatchItems = func_get_args();
        $this->nitems = func_num_args();
        $this->watchList = array();
        
        if($this->nitems) {
            foreach($this->WatchItems as $watch) {
                $this->watchList[] = $watch->getItem2Watch();
            }
        }
    }

    public function CreateJSON(): int
    {
        return file_put_contents($_SERVER["DOCUMENT_ROOT"].self::WATCH_JSON, json_encode($this->watchList));
    }
};
?>