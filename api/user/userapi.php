<?php

namespace Api\App;

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "api/database/databaseconnection.php";
require_once "app/core/hash.php";
require_once "app/core/password.php";

use Api\Physical\Database;
use App\Core\Hash;
use App\Core\Password;

class UserAPI
{
    private const QUERY_USERS =
        "SELECT
            id,
            username
        FROM project_users";

    private const QUERY_USER = 
        "SELECT
            since,
            username,
            email
        FROM project_users
        WHERE project_users.id = ?";

    private const QUERY_USER_PASSWORD_HASH = 
        "SELECT
            SHA2(
                SHA2(
                    CONCAT(
                        SHA1(email),
                        SHA1(username),
                        SHA1(
                            CONCAT(
                                CASE
                                    WHEN DAYOFWEEK(since) = 1 THEN 'Sunday'
                                    WHEN DAYOFWEEK(since) = 2 THEN 'Monday'
                                    WHEN DAYOFWEEK(since) = 3 THEN 'Tuesday'
                                    WHEN DAYOFWEEK(since) = 4 THEN 'Wednesday'
                                    WHEN DAYOFWEEK(since) = 5 THEN 'Thursday'
                                    WHEN DAYOFWEEK(since) = 6 THEN 'Friday'
                                    WHEN DAYOFWEEK(since) = 7 THEN 'Saturday'
                                END,
                                ' ',
                                since
                            )
                        ),
                        id,
                        IFNULL(SHA1(password_hash), '')
                    ),
                512),
            256) AS user_hash
        FROM project_users
        WHERE project_users.id = ?";

    private const QUERY_HASHED_USER = 
        "SELECT
            id
        FROM project_users
        WHERE 
            SHA2(
                SHA2(
                    CONCAT(
                        SHA1(email),
                        SHA1(username),
                        SHA1(
                            CONCAT(
                                CASE
                                    WHEN DAYOFWEEK(since) = 1 THEN 'Sunday'
                                    WHEN DAYOFWEEK(since) = 2 THEN 'Monday'
                                    WHEN DAYOFWEEK(since) = 3 THEN 'Tuesday'
                                    WHEN DAYOFWEEK(since) = 4 THEN 'Wednesday'
                                    WHEN DAYOFWEEK(since) = 5 THEN 'Thursday'
                                    WHEN DAYOFWEEK(since) = 6 THEN 'Friday'
                                    WHEN DAYOFWEEK(since) = 7 THEN 'Saturday'
                                END,
                                ' ',
                                since
                            )
                        ),
                        id,
                        IFNULL(SHA1(password_hash), '')
                    ),
                512),
            256) = '?'";

    private const QUERY_CHANGE_PASSWORD_USER =
        "UPDATE project_users SET password_hash = '?' WHERE id = ?";

    /* Database $Database */
    private $Database;

    /* array $user */
    private $user;

    public function __construct()
    {
        self::setDatabase();
    }

    public function __destruct()
    {
        unset($this->Database);
    }

    protected function setDatabase()
    {
        if(!isset($GLOBALS["hostname"], $GLOBALS["username"], $GLOBALS["password"], $GLOBALS["db_name"], $GLOBALS["DBerrorMessage"])) {
            throw new DatabaseException("Could not connect to the database");
        }

        $this->Database = new Database($GLOBALS["hostname"], $GLOBALS["username"], $GLOBALS["password"], $GLOBALS["db_name"], $GLOBALS["DBerrorMessage"]);
    }

    public function isValidUserId(int $id): bool
    {
        $result = $this->Database->ResultToArray(self::QUERY_USERS);
        return empty($result) || !is_array($result) ? null : in_array($id, array_keys($this->Database->PairOptions($result)), true);
    }

    public function EnlistUsers()
    {
        $result = $this->Database->ResultToArray(self::QUERY_USERS);
        $this->user = empty($result) || !is_array($result) ? null : $this->Database->PairOptions($result);
    }

    public function getUserFromId(int $id)
    {
        if(!self::isValidUserId($id)) {
            $this->user = null;
            return;
        }

        $result = $this->Database->ResultToArray(str_replace("?", $id, self::QUERY_USER));
        $this->user = empty($result) || !is_array($result) ? null : $result[0];
    }

    public function getIdFromHashedUser(string $hash)
    {
        if(!Hash::isUserHash($hash)) {
            $this->user = null;
            return;
        }

        $result = $this->Database->ResultToArray(str_replace("?", $hash, self::QUERY_HASHED_USER));
        $this->user = empty($result) || !is_array($result) ? null : array_values($result[0])[0];
    }

    public function getHashFromUserId(int $id)
    {
        if(!self::isValidUserId($id)) {
            $this->user = null;
            return;
        }

        $result = $this->Database->ResultToArray(str_replace("?", $id, self::QUERY_USER_PASSWORD_HASH))[0];
        $this->user = empty($result) || !is_array($result) ? null : array_values($result)[0];
    }

    public function getUserNameFromId(int $user_id, bool $hash = false)
    {
        if(!self::isValidUserId($user_id)) {
            $this->user = null;
            return;
        }

        $result = $this->Database->ResultToArray(str_replace("?", $user_id, self::QUERY_USER));
        $this->user = empty($result) || !array($result) ? null : array_column($result, "username")[0];

        if(!empty($this->user) && $hash) {
            $username = $this->user;
            self::getHashFromUserId($user_id);
            $this->user = array("name" => $username, "hash" => $this->user);
        }
    }

    public function JSON(): string
    {
        return htmlspecialchars_decode(json_encode($this->user, JSON_FORCE_OBJECT | JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES));
    }

    public function ChangePassword(string $user_hash, Password $Password): bool
    {
        self::getIdFromHashedUser($user_hash);

        if(empty($this->user) || !is_numeric($this->user) || !$Password->isStrongPassword()) {
            return false;
        }

        $query = str_replace("?", Hash::Password($Password), preg_replace("/\?$/", $this->user, self::QUERY_CHANGE_PASSWORD_USER));

        return $this->Database->PerformQuery($query);
    }
};
?>