<?php

session_start();

$index_page = true;
$FileDate = new DateTime("@".strval(filemtime("index.php")));
$year = $FileDate->format("Y");

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "api/user/userapi.php";
require_once "app/core/password.php";

use Api\App\UserAPI;
use App\Core\Password;

$UserAPI = new UserAPI();

if($_GET) {
    if(array_key_exists("id", $_GET)) {
        $UserAPI->getUserFromId(intval($_GET["id"]));
        echo $UserAPI->JSON();
    } elseif(array_key_exists("hash", $_GET)) {
        if(array_key_exists("pwd", $_GET)) {
            echo $UserAPI->ChangePassword($_GET["hash"], new Password($_GET["pwd"]));
        } else {
            $UserAPI->getIdFromHashedUser($_GET["hash"]);
            echo $UserAPI->JSON();
        }

    /* SESSION DEPENDED API */
    } elseif(array_key_exists("session", $_GET) && $_SESSION) {
        if($_GET["session"] == "login" && array_key_exists("login", $_SESSION)) {
            if(array_key_exists("user", $_GET) && array_key_exists("user", $_SESSION["login"]) && array_key_exists("id", $_SESSION["login"]["user"])) {
                if($_GET["user"] == "hash") {
                    $UserAPI->getHashFromUserId(intval($_SESSION["login"]["user"]["id"]));
                    echo $UserAPI->JSON();
                } elseif($_GET["user"] == "id") {
                    echo $_SESSION["login"]["user"]["id"];
                } elseif($_GET["user"] == "name") {
                    $UserAPI->getUserNameFromId($_SESSION["login"]["user"]["id"], true);
                    echo $UserAPI->JSON();
                }
            }
        } else {
            echo "null";
        }
    }
} else {
    $UserAPI->EnlistUsers();
    echo $UserAPI->JSON();
}

unset($UserAPI);
?>