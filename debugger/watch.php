<?php

if(!isset($_SESSION)) exit;

require_once "app/core/errorhandling.php";
require_once "api/watch/watch.php";

use Debug\Watch;
use Debug\WatchItem;
use App\Core\ErrorHandling;
use App\Core\ErrorMessage;

if(!file_exists("shared/logs/")) {
    if(!mkdir("shared/logs/", 0777, true)) exit;
}

$ErrorLogFile = new Files\File("shared/logs/", "errors.log");
$ErrorHandling = new App\Core\Error\ErrorHandling($ErrorLogFile, !$_SESSION["local"]);

if(!$_SESSION["local"] && !($_SESSION["using_ssl"] = ($_SESSION["protocol"] === "https"))) {
	$ErrorMessage = new ErrorMessage("For the use of this CMS, a SSL-encryption certificated is required. Check the web domain settings from your hoster.\nThe website stopped loading...", debug_backtrace());
	$ErrorHandling->AddMessage($ErrorMessage);
	$ErrorHandling->KillSystem();
}

$Watch = "";
if ($_GET) {
    if (array_key_exists("watch", $_GET) && $_SESSION["local"]) {
        $Watch = new Watch(
            new WatchItem("_SESSION", $_SESSION),
            new WatchItem("_SERVER", $_SERVER)
        );
        if ($Watch->CreateJSON() === false) {
            $errormessage = "Failed to write " . $Watch::WATCH_JSON . " file.\n";
            if (file_exists($Watch::WATCH_JSON)) {
                $errormessage .= "Something with file permissions perhaps?\n";
            } else {
                $errormessage .= "File does not exist or could not find it.\n";
            }
            $errormessage .= "Application stopped!";

            $ErrorMessage = new ErrorMessage(ErrorMessage::APP_SYSTEMERROR, ErrorMessage::ERROR_ERRORTYPE, 6, $errormessage, debug_backtrace());
            $ErrorHandling->AddMessage($ErrorMessage);
            $ErrorHandling->KillSystem();
        }
    }
}
?>