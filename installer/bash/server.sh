#!/bin/bash
if [[ "$EUID" -ne 0 ]]; then
    echo "Please run this script as root"
    exit 1
fi

# Make some directories accessable for the server to write to
chown www-data:www-data ../api/json
chown www-data:www-data ../shared/logs
chown www-data:www-data ../webcontent/site

# Hide MySQL password
# See: https://stackoverflow.com/questions/8354777/how-i-can-make-read-doesnt-show-its-value-in-shell-scripting
echo "Enter MySQL root password: "
stty -echo
read DATABASE_PASS
stty echo

# MySQL commands from Linux Shell see:
# See: https://www.shellhacks.com/mysql-run-query-bash-script-linux-command-line/
# $ mysql -u USER -pPASSWORD -e "SQL_QUERY"
# Set MySQL Create MySQL Database

echo "Create database"
mysql -u root -p"$DATABASE_PASS" -e "CREATE DATABASE IF NOT EXISTS project CHARACTER SET utf8 COLLATE utf8_bin"

echo "Create user";
mysql -u root -p"$DATABASE_PASS" -e "CREATE USER 'project'@'localhost' IDENTIFIED BY 'user_password'"
mysql -u root -p"$DATABASE_PASS" -e "GRANT ALL ON project.* TO 'project'@'localhost'"
#mysql -u root -p"$DATABASE_PASS" -e "GRANT ALL ON project TO 'project'@'%'"
#mysql -u root -p"$DATABASE_PASS" -e "GRANT CREATE ON project TO 'project'@'%'"
mysql -u root -p"$DATABASE_PASS" -e "FLUSH PRIVILEGES"

echo "Build up database"
mysql -u root -p"$DATABASE_PASS" < ../sql/project.sql