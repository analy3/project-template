/*
*
* @project		:	*project
*
* @description	:	
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	
*
*
*
* @content		:
* 					1. Select Database
* 					2. Delete tables if present
* 					3. Create tables
* 					4. Fill tables
*					5. Select data (Deprecated)
*
* @remarks		:	Load file
*					UNIX : $ mysql -u user -p[password] < dir/file.sql
*					MYSQL: mysql> source dir/file.sql
*
* @change log	:
*
*/

-- 1. Selecteer databank

USE project;




-- 2. Verwijder tabellen mits aanwezig

DROP TABLE IF EXISTS bezoeken;
DROP TABLE IF EXISTS robots;






-- 3. Maak tabellen aan

CREATE TABLE IF NOT EXISTS updates (
	id int(11) NOT NULL AUTO_INCREMENT,
	updated_at timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	key(updated_at),
	primary key(id)
);


CREATE TABLE `table` (
	id int(11) NOT NULL AUTO_INCREMENT,

	primary key(id)
);


CREATE TABLE robots (
	id int(11) NOT NULL AUTO_INCREMENT,
	regex varchar(100) NOT NULL UNIQUE,

	primary key(id)
);

CREATE TABLE bezoeken (
	id int(11) NOT NULL AUTO_INCREMENT,
	stamp timestamp NOT NULL,
	host_ip varchar(20),
	host_hostname varchar(255),
	host_city varchar(255),
	host_region varchar(255),
	host_country varchar(50),
	host_location_NW double,
	host_location_ZO double,
	host_company varchar(255),
	host_postal_code varchar(10),
	client_ip varchar(20),
	client_visited varchar(750),
	client_useragent varchar(255),

	primary key(id)
);





-- 4. Vul de tabellen
SET sql_mode = '';

INSERT INTO updates () VALUES
();

