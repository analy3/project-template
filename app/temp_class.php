<?php
/*
*
* @project		:	
*
* @description	:	
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	
*
* @launch		:
*
*
*
* @class		:	Class( $member )
*
* @members		:	- = private; + = public; o = static
*
* @includes		:	
*
* @remarks		:
*
* @change log	:
*
*/

class OOPClass
{
	/* int $member */
	private $member;

	public function __construct(int $member)
	{
		$this->member = $member;
	}

	public function method()
	{
		return $this->member;
	}
};
?>