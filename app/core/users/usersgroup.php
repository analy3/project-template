<?php
namespace App\Core\Users;

define("DEFAULT_WWW_USERGROUP", "www-data");

class UsersGroup
{
	private $usersGroup;

	public function __construct($usersGroup = DEFAULT_WWW_USERGROUP)
	{
		if(!is_string($usersGroup)) {
			die("Users Group: Invalid Argument");
		}

		$this->usersGroup = $usersGroup;
	}

	public function getUsersGroup()
	{
		return $this->usersGroup;
	}
}
?>
