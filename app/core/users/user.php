<?php
namespace App\Core\Users;

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "app/core/users/users_group.php";

class User
{
	private $user;
	private $usersGroup;

	public function __construct($user = DEFAULT_WWW_USERGROUP, UsersGroup $usersGroup = null)
	{
		if(!is_string($user)) {
			die("User: Invalid Argument");
		}

		$this->user = $user;
		$this->usersGroup = empty($usersGroup) ? new UsersGroup(DEFAULT_WWW_USERGROUP) : $usersGroup;
	}

	public function getUser()
	{
		return $this->user;
	}

	public function getUsersGroup()
	{
		return $this->usersGroup->getUsersGroup();
	}
}
?>
