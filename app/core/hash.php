<?php
/*
*
* @project		:	LAVENDAR SITE MAJOR - CONTENT MANAGEMENT SYSTEM
*
* @description	:	Another CMS
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <mike@vandartel-kniknie.nl>
*
*
*
* @date			:	Saturday, 3 April 2021
*
* @launch		:	
*
*
*
* @content		:	May give a hash string
*
*/

namespace App\Core;

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "app/core/emailaddress.php";

use App\Core\EmailAddress;

class Hash
{
	const REGEX_HASH256 = "/^[0-9a-f]{64}$/";
	const REGEX_HASH512 = "/^[0-9a-f]{128}$/";
    const REGEX_WHIRLPOOL = self::REGEX_HASH512;

	static function isUserHash(string $hash): bool
	{
		return preg_match(self::REGEX_HASH256, strtolower($hash));
	}

    static function User(int $user_id, EmailAddress $EmailAddress, string $username, \DateTime $since, string $password_hash = ""): string
    {
        $password_hash = empty($password_hash) ? "" : hash("sha1", $password_hash);

        $composed_hash = hash("sha256", hash("sha512", 
            hash("sha1", $EmailAddress->getEmailAddress()).
            hash("sha1", $username).
            hash("sha1", $since->format("l")." ".preg_replace("/\+\d{1,2}\:\d{2}$/", "", str_replace("T", " ", $since->format("c")))).
            strval($user_id).
            $password_hash
        ));

        return $composed_hash;
    }

    static function isFormHash(string $hash): bool
    {
        return preg_match(self::REGEX_WHIRLPOOL, strtolower($hash));
    }

    static function Form(string $form_name, string $page_name, string $user_name): string
    {
        return hash("whirlpool", hash("sha512", $form_name.$page_name.$user_name));
    }

    static function isURLHash(string $hash): bool
    {
        return preg_match(self::REGEX_HASH256, strtolower($hash));
    }

    static function Password(Password $Password, int $cost = 14): string
    {
        return $Password->Hash($cost);
    }

    static function isCorrectPassword(Password $password, string $hash): bool
    {
        return $Password->Verify($hash);
    }
};
?>