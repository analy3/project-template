<?php
/*
*
* @project		:	LAVENDAR SITE MAJOR - CONTENT MANAGEMENT SYSTEM
*
* @description	:	Another CMS
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <mike@vandartel-kniknie.nl>
*
*
*
* @date			:	Saturday, 3 April 2021
*
* @launch		:	
*
*
*
* @content		:	Password value object
*
*/

namespace App\Core;

class Password
{
    const REGEX_STRONG_PASSWORD = "/^((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,})$/";

    /* string $password */
    private $password;

    public function __construct(string $password)
    {
        $this->password = $password;
    }

    public function isStrongPassword(): bool
    {
        return preg_match_all(self::REGEX_STRONG_PASSWORD, $this->password);
    }

    public function Hash(int $cost): string
    {
        if($cost < 10) return "";

        return password_hash($this->password, PASSWORD_DEFAULT, array("cost" => $cost));
    }

    public function Verify(string $hashed_password): bool
    {
        return password_verify($this->password, $hashed_password);
    }
};
?>