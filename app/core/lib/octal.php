<?php

namespace Lib;

class Octal
{
	const REGEX_OCTAL_NUMBER = "/^0?[0-7]+$/";
	const ESCAPE = "%o";

	private $octal;

	public function __construct($octal)
	{
		if(self::isOctal($octal)) {
			$this->octal = $octal;
		} elseif(is_numeric($octal) && preg_match(self::REGEX_OCTAL_NUMBER, $octal)) {
			$this->octal = octdec($octal);
		} else {
			die("Octal: Invalid agrument");
		}
	}

	public function isOctal($number)
	{
	    return decoct(octdec($number)) == $number;
	}

	public function toString()
	{
		return decoct($this->octal);
	}

	public function getOctal($octal = false)
	{
		return ($octal === false ? $this->octal : intval(decoct($this->octal)));
	}

	public function minOctal(Octal $octal)
	{
		return $this->octal >= $octal->getOctal();
	}

	public function maxOctal(Octal $octal)
	{
		return $this->octal <= $octal->getOctal();
	}
}
?>
