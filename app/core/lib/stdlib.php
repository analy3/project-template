<?php
define("REGEX_HEXADECIMAL_NUMBER", "/[0-9a-fA-F]+/");

function number(&$value)
{
	if($return = is_numeric($value)) {
		$value = (is_int($value) ? intval($value) : floatval($value));
	}

	return $return;
}


# Numeric string with leading zeros

function leadingZeros($number, $maxlength = 0)
{
	if(!is_numeric($number) || !number($maxlength)) return null;

	if(($maxlength = intval($maxlength)) <= 0) {
		return str_repeat(abs($maxlength), "0").$number;
	}

	if(!is_string($number)) {
		$number = number_format($number, 0, "", "");
	}

	return str_repeat("0", $maxlength - strlen($number)).$number;
}


# Hexadecimal with leading zeros

function leadingZerosHex($hexNumber, $maxlength = 0, $escape = false)
{
	if(is_string($hexNumber) && preg_match(REGEX_HEXADECIMAL_NUMBER, $hexNumber)) {

		if(($maxlength = intval($maxlength)) <= 0) {
			return str_repeat(abs($maxlength), "0").$hexNumber;
		}

		return str_repeat("0", $maxlength - strlen($hexNumber)).$hexNumber;
	}

	return ($escape ? leadingZeros($hexNumber, $maxlength) : null);
}
?>
