<?php


# Get nth element of the array

function index($array, $index)
{
	if(is_array($array) && is_int($index) && $index >= 0 && $index < count($array)) {
		$result = array_keys($array);

//		echo $result[$index];

		return $result[$index];
	}

	if(!is_array($array)) return -1;
	if(!is_int($index)) return -2;
	if($index < 0) return -3;
	if($index >= count($array)) return -4;
}


# Maps changes between keys and their values of an array

function array_changes($array)
{
	$diff_keys = array_diff_assoc(array_keys($array), array_values($array));
	$diff_values = array_diff_assoc(array_values($array), array_keys($array));

	if(count($diff_keys) == 0) {
		return $diff_values;
	}

	if(count($diff_values) == 0) {
		return $diff_keys;
	}

	if(count($diff_keys) == count($diff_values)) {
		return array_combine($diff_keys, $diff_values);
	}

	return array();
}


# Match every element in an array by a pattern 

function array_preg_match_values($pattern, $array, &$matches = array(), $match_all = false, $recursive = false)
{
	if(!is_string($pattern) || !is_array($array) || !preg_match(REGEX_PATTERN_STRING, $pattern)) {
		$matches = null;
		return false;
	}

	$n = count($array);
	if(!$n) {
		$matches = array();
		return false;
	}

	if($match_all !== false) {
		$match_all = true;
	}

	if($recursive !== false) {
		$recursive = true;
	}

	if($regex_g_modifier = preg_match(REGEX_PATTERN_MATCH_ALL, $pattern)) {
		$offset = strrpos($pattern, "/");
		$pattern_without_g_modifier = substr($pattern, 0, $offset) . str_replace("g", "", substr($pattern, $offset));
	}

	$found = false;
	foreach($array as $key => $value) {
		if($found && !$match_all) {
			break;
		}

		$temp_matches = array();
		if(is_array($value) && $recursive) {

			if($intermediate = array_preg_match_values($pattern, $value, $temp_matches, $match_all, $recursive)) {
				$matches[$key] = $temp_matches;
			}

			$found = ($found || $intermediate);

		} elseif(is_string($value)) {

			if($intermediate = ($regex_g_modifier ? preg_match_all($pattern_without_g_modifier, $value, $temp_matches) : preg_match($pattern, $value, $temp_matches))) {
				$matches[$key] = $temp_matches;//(count($temp_matches) > 1 ? $temp_matches : $temp_matches[0]);
			}

			$found = ($found || $intermediate);

		}
	}

	return $found;
}

?>
