<?php
/*
*
* @project		:	LAVENDAR SITE MAJOR - CONTENT MANAGEMENT SYSTEM
*
* @description	:	Another CMS
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <mike@vandartel-kniknie.nl>
*
*
*
* @date			:	Monday, 8 March 2021
*
* @launch		:	
*
*
*
* @content		:	May give a version
*
*/

namespace Lib;

class Version
{
    /* mysql $database */
    private $database;

    /* array $versions */
    private $version;

    public function __construct(mysql $database)
    {
        $this->database = $database;
        $this->versions = array();
        $this->versions["php"] = phpversion();

        if(!empty($this->database)) {
            $this->versions["mysql"] = mysqli_get_server_info($this->database);
        }
    }

    static function human_readable_version(string $versionstring): string
    {
        if(!is_string($versionstring)) return null;

        return substr($versionstring, 0, strpos($versionstring, ".", 2));
    }

    public function getPHP($human_readable = false): string
    {
        if(!array_key_exists("php", $this->versions)) return null;

        return $human_readable ? self::human_readable_version($this->versions["php"]) : $this->versions["php"];
    }

    public function getMySQL($human_readable = false): string
    {
        if(!array_key_exists("mysql", $this->versions)) return null;

        return $human_readable ? self::human_readable_version($this->versions["mysql"]) : $this->versions["mysql"];
    }
};
?>