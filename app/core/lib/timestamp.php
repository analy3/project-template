<?php

namespace Lib;

class TimeStamp
{
	const REGEX_TIMESTAMP = "/\d{4}\-(0[1-9]|1[0-2])\-(0[1-9]|[12][0-9]|3[0-1])T([01][0-9]|2[0-3])\:[0-5][0-9]\:[0-5][0-9](\+|\-)(0[0-9]|1[0-2])\:(00|30)/";
	private $timestamp;

	public function __construct($timestamp = null)
	{
		if($timestamp == null) {
			$this->timestamp = time();

		} elseif(is_int($timestamp)) {
			$this->timestamp = $timestamp;

		} elseif(is_string($timestamp) && preg_match(self::REGEX_TIMESTAMP, $timestamp)) {
			$this->timestamp = strtotime($timestamp);

		} else {
			die("Invalid timestamp");
		}
	}

	public function getTimeStampString()
	{
		return date("c", $this->timestamp);
	}

	public function getTimeStampInt()
	{
		return $this->timestamp;
	}
}
?>
