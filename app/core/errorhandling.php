<?php
/*
*
* @project		:	LAVENDAR SITE MAJOR - CONTENT MANAGEMENT SYSTEM
*
* @description	:	Another CMS
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <mike@vandartel-kniknie.nl>
*
*
*
* @date			:	Monday, 8 March 2021
*
* @launch		:
*
*/

namespace App\Core\Error;

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "app/core/files/file.php";

use Files\File;

class ErrorMessage
{
    const APP_SYSTEMERROR = 0;
    const PHP_SYSTEMERROR = 1;
    const MYSQL_SYSTEMERROR = 2;
    
    const SUCCESS_ERRORTYPE = 0;
    const NOTICE_ERRORTYPE = 1;
    const WARNING_ERRORTYPE = 2;
    const ERROR_ERRORTYPE = 3;
    
    /* DateTime $Timestamp */
    private $Timestamp;

    /* int $systemerror */
    private $systemerror;

    /* int $errortype */
    private $errortype;

    /* IP $client */
    //private $client;

    /* array $backtrace */
    private $backtrace;

    /* int $errornumber */
    private $errornumber;

    /* string $message */
    private $message;

    public function __construct(int $systemerror, int $errortype, int $errornumber, string $message, array $backtrace = null)
    {
        $this->Timestamp = new \DateTime();
        $this->Timestamp->getTimestamp();
        $this->backtrace = $backtrace;
        $this->systemerror = $systemerror;
        $this->errortype = $errortype;
        $this->errornumber = $errornumber;
        $this->message = $message;
    }

    public function getSystemError(): int
    {
        return $this->systemerror;
    }

    public function getSystemErrorStringed(): string
    {
        switch($this->systemerror) {
            case self::APP_SYSTEMERROR: return "app";
            case self::PHP_SYSTEMERROR: return "php";
            case self::MYSQL_SYSTEMERROR: return "mysql";
        }
    }

    public function getErrorType(): int
    {
        return $this->errortype;
    }

    public function getErrorTypeStringed(): string
    {
        switch($this->errortype) {
            case self::SUCCESS_ERRORTYPE: return "success";
            case self::NOTICE_ERRORTYPE: return "notice";
            case self::WARNING_ERRORTYPE: return "warning";
            case self::ERROR_ERRORTYPE: return "error";
        }
    }

    public function getErrorNumber(): int
    {
        return $this->errornumber;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function toString(): string
    {
        return "[" . $this->Timestamp->format("d/m/Y H:i:s") . "] [ " . strtoupper(self::getsystemerrorStringed()) . " ] [ " . ucfirst(self::getErrorTypeStringed()) . " ] [" . $this->errornumber . "] [ " . $this->message . " ] [" . (empty($this->backtrace) ? "NO BACKTRACE HAS BEEN SET" : print_r($this->backtrace, true)) . "]";
    }

    public function toArray(): array
    {
        return array($this->Timestamp->format("c"), self::getsystemerrorStringed(), self::getErrorTypeStringed(), $this->errornumber, nl2br($this->message), $this->backtrace);
    }
};

class ErrorHandling
{
    const REGEX_TIMESTAMP_IN_STRING = "/^\[(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{,4}\s([01][0-9]|2[0-3]):[0-5][0-9]\:[0-5][0-9]\]\s/";
    const ERROR_JSON = "../../api/json/errors.json";

    /* File $LogFile */
    private $LogFile;

    /* array $messages */
    private $messages;

    /* ErrorMessage $ErrorMessage */
    private $ErrorMessage;

    public function __construct(File $LogFile = null)
    {
        $this->LogFile = $LogFile;
        $this->messages = array();
        if(file_exists(self::ERROR_JSON)) {
            file_put_contents(self::ERROR_JSON, "");
        }
    }

    private function AppendToLogFile(): int
    {
        return file_put_contents(
            $this->LogFile->Path(),
            ($this->LogFile->Exists() && empty($this->messages) ? "----------------------------------------------------------------------------------\n" : "") . $this->ErrorMessage->toString() . "\n",
            ($this->LogFile->Exists() ? FILE_APPEND : 0)
        );
    }

    public function AddMessage(ErrorMessage $Message): int
    {
        $result = -1;

        if(!empty($this->LogFile)) {
            $this->ErrorMessage = $Message;
            $result = self::AppendToLogFile();
        }

        $this->messages[] = $Message;

        return $result;
    }

    public function toArray(): array
    {
        if(empty($this->messages)) return null;

        $message_array = array();
        foreach($this->messages as $MessageValue) {
            $message_array[] = array_combine(array("timestamp", "system", "type", "number", "message", "backtrace"), $MessageValue->toArray());
        }

        return $message_array;
    }

    public function CreateJSON(): int
    {
        return file_put_contents(self::ERROR_JSON, json_encode(self::toArray()));
    }

    public function KillSystem()
    {
        self::CreateJSON();
        die;
    }
};
?>