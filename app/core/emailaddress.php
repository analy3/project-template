<?php
/*
*
* @project		:	LAVENDAR SITE MAJOR - CONTENT MANAGEMENT SYSTEM
*
* @description	:	Another CMS
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <mike@vandartel-kniknie.nl>
*
*
*
* @date			:	Saturday, 3 April 2021
*
* @launch		:	
*
*
*
* @content		:	Email address value object
*
*/

namespace App\Core;

class EmailAddress
{
    /* string $address */
    private $address;

    public function __construct(string $address)
    {
        if(!self::isValidEmailAddress($address)) {
            throw new InvalidArgumentException("Email object says: Invalid e-mail address");
        }

        $this->address = $address;
    }

    public function  __destruct ()
    {
        
    }

    static function isValidEmailAddress(string $address): bool
    {
        return filter_var($address, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function getEmailAddress(): string
    {
        return $this->address;
    }
}
?>