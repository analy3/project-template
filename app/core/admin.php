<?php
/*
*
* @project		:	LAVENDAR SITE MAJOR - CONTENT MANAGEMENT SYSTEM
*
* @description	:	Another CMS
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <mike@vandartel-kniknie.nl>
*
*
*
* @date			:	Monday, 8 March 2021
*
* @launch		:	
*
*
*
* @content		:	Collection of database queries with the required PHP-functions, constants and objects
*
*/


if(!isset($index_page)) {
	header("Location: /project");
	exit;
}

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "app/core/server.php";
require_once "api/user/userapi.php";

use Api\App\UserAPI;

if(array_key_exists("login", $_SESSION) && array_key_exists("user", $_SESSION["login"]) && array_key_exists("id", $_SESSION["login"]["user"])) {
	$UserAPI = new UserAPI();
	$UserAPI->getUserFromId($_SESSION["login"]["user"]["id"]);
	$_SESSION["login"]["user"] = array_merge($_SESSION["login"]["user"], \Api\Physical\Database::DecodeJSON($UserAPI->JSON()));
	unset($UserAPI);
}
?>