<?php

namespace Files;

class FileOwner
{
	private $owner;

	public function __construct(string $fileOwner)
	{
		$this->owner = $fileOwner;
	}

	public function getFileOwner(): string
	{
		return $this->owner;
	}
}
?>
