<?php

namespace Files;

/*
File Express is a file application concept where the idea is that the file domain should be less technical.
The concept is an interface between coding and file levels.

Such as:
- No file extentions
- No path
*/

namespace Files;

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "app/core/files/directory.php";
require_once "app/core/lib/octal.php";

use Lib\Octal;

class FileExpress extends Directory
{
	private $rootDir;

/*
string basename ( string $path [, string $suffix ] )
*/

	public function __contruct(string $rootDir)
	{
		$this->rootDir = new Directory($rootDir, new FileRights(new Octal(0755)));
	}

	public function delete(string $dir, string $file): int
	{
		$merged_dir_filename = filepath($dir, find($file, $dir));

		if(!file_exists($merged_dir_filename)) {
			return -1;
		}

		if(!in_array(file_permissions($merged_dir_filename), array(NORM_FILE_PERM, ALL_FILE_PERM), true)) {
			return -2;
		}

		return unlink($merged_dir_filename);
	}

	public function rename(string $dir, string $old, string $new)
	{
		if(!file_exists($dir)) {
			return -1;
		}

		$ext = add_file_type_extention($dir, $old, false);

		$temp = $new;
		$new = filepath($dir, strchop($new).".".$ext);
		if(file_exists($new)) {
			$new = filepath($dir, strchop($temp).RENAME.".".$ext);
		}
		$old = filepath($dir, find($old, $dir));

		if(!file_exists($old)) {
			return -5;
		}

		if(!in_array(permissions($old), array(NORM_FILE_PERM, ALL_FILE_PERM), true)) {
			return -6;
		}

		return rename($old, $new);
	}

	function find(string $filename_to_search_for, string $dir)
	{
		if(!file_exists($dir) && !is_dir($dir)) {
			return null;
		}

		$files = map_files_filter($dir, FILES_ONLY);

		if(!count($files)) {
			return false;
		}

		$filename_to_search_for = strchop($filename_to_search_for);
		$names = array_map("strchop", $files);

		if(!in_array($filename_to_search_for, $names)) {
			return false;
		}

		$result = array();
		foreach($names as $index => $name) {
			if($name === $filename_to_search_for) {
				$result[$index] = $files[$index];
			}
		}

		if(count($result) != 1) {
			return $result;
		}

		return $files[key($result)];
	}
}
?>
