<?php

namespace Files;

require_once "files/directory.php";

class portfolio
{
	const PORTFOLIO = "portfolio";
	private $portfolio;

	public function __construct()
	{
		$this->portfolio = new Directory(self::PORTFOLIO, new FileRights());
	}
}
?>
