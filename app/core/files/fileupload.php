<?php

namespace Files;

require_once "files/file_manipulation.php";

class FileUpload extends FileManipulation
{
	private $files;
	private $uploads;
	private $destination;

	public function __construct(Directory $destination)
	{
		if(!$_FILES || !is_array($this->uploads = self::multipleUpload())) {
			die("File Upload: No Files Known");
		}

		foreach($this->uploads as $index => $file) {
			if(self::isValid($file)) {
				$this->files[] = new File($destination->path(), $file);
			} else {
				die("File Upload: Invalid Upload. ".$index." Files Uploaded");
			}
		}
	}

	private function multipleUpload(): array
	{
		if(!is_array($_FILES) || 
			!array_key_exists("name", $_FILES) ||
			!array_key_exists("type", $_FILES) ||
			!array_key_exists("tmp_name", $_FILES) ||
			!array_key_exists("error", $_FILES) ||
			!array_key_exists("size", $_FILES)
		) {
			return null;
		}

		$n = count($_FILES["name"]);
		$file_array = array();
		foreach($_FILES as $key => $files) {
			if($n) {
				if($n != count($files)) {
					die("Files Upload: Error Unequal Indexes");
				}

				foreach($files as $number => $value) {
					$file_array[$number][$key] = $value;
				}
			} else {
				for($number = 0; $number < $n; $number++) {
					$file_array[$number][$key] = $value;
				}
			}
		}

		return $file_array;
	}

	public function isValid(array $file, $mimetype = null)
	{
		return empty(array_diff_key(array_flip(array("name", "type", "tmp_name", "error", "size")), $file)) &&
			!empty($file["name"]) &&
			strlen($file["name"]) &&
			(empty($mimetype) ? true : $file["type"] == $mimetype) &&
			!empty($file["tmp_name"]) &&
			!$file["error"] &&
			$file["size"];
	}

	public function upload()
	{
		$result = true;
		foreach($this->uploads as $file) {
			$result = ($result ? move_uploaded_file($file["tmp_name"], $this->destination->path()) : false);
		}

		return $result;
	}
}
?>
