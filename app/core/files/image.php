<?php

namespace Files;

require_once "files/file.php";

class Image extends File
{
	const DEFAULT_IMAGE_RIGHTS = 0644;

	private $image;

	public function __construct(array $image)
	{
		$this->image = $image;
		if(!self::isValidUpload()) {
			die("Invalid upload");
		}
	}

	function isValidUpload()
	{
		return is_array($this->image) &&
			empty(array_diff_key(array_flip(array("name", "type", "tmp_name", "error", "size")), $this->image)) &&
			!empty($this->image["name"]) &&
			strlen($this->image["name"]) &&
			preg_match(REGEX_MIME_IMAGE, $this->image["type"]) &&
			!empty($this->image["tmp_name"]) &&
			!$this->image["error"] &&
			$this->image["size"];		
	}

}
?>
