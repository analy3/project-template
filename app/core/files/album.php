<?php

namespace Files;

require_once "files/portfolio.php";
require_once "lib/stdlib.php";

class album
{
	const NAMELESS = "nameless";
	const HIDDEN_MARK = "*";

	private $album;
	private $title;
	private $order;
	private $hidden;

	public function __construct(string $title = self::NAMELESS, int $order = 0, bool $hidden = true)
	{
		$this->title = $title;
		$this->order = $order;
		$this->hidden = $hidden;

		$album = new Directory(new File(self::path(Portfolio::PORTFOLIO, leadingZero($order, 3)."_".$title.($this->hidden ? self::HIDDEN_MARK : ""))));
	}
}
?>
