<?php

namespace Files;

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "app/core/lib/timestamp.php";

use Lib\TimeStamp;

class File
{
    const FILE_ACCESS_TIME = 0;
    const FILE_CHANGE_TIME = 1;
    const FILE_MODIFY_TIME = 2;

    const PARSE_INI_STRING = 0;
    const PARSE_INI_ARRAY = 1;
    const PARSE_INI_JSON = 2;

    const REGEX_FILENAME = "/[0-9a-zA-Z\ \-\_\.\~]+/";
    const REGEX_HIDDEN_FILES = "/^\./";

    const REGEX_MIME_TEXT = "/^text\/plain$/";
    const REGEX_MIME_HTML = "/^text\/html$/";
    const REGEX_MIME_XHTML = "/^application\/x(ht)?ml$/";
    const REGEX_MIME_IMAGE = "/^image\//";

    const REGEX_PATTERN_STRING = "/^\/(.+)\/([gm]?i[gm]?|[im]?g[im]?|[ig]?m[ig]?)?$/";
    const REGEX_PATTERN_MATCH_ALL = "/^\/(.+)\/([im]?g[im]?)$/";

	/* string $location */
	private $location;

	/* string $file */
	private $file;

	public function __construct(string $location = null, string $filename = null)
	{
		if(!empty($location) && !is_dir($location)) {
			throw new \InvalidArgumentException("File: Invalid argument for location");
		}

		self::setLocation($location);
		$this->file = is_null($filename) ? "" : self::Name($filename);
	}

	private function setLocation(string $location)
	{
		if(!is_dir($location)) {
			throw new \InvalidArgumentException("File object says that directory does not exist!");
		}
		
		$this->location = empty($location) ? "./" : ($location.(preg_match("/\/$/", $location) ? "" : "/"));
	}

	private function Name(string $filename): string
	{
		return (empty($filename) ? "" : preg_replace("/^\/?/", "", $filename));
	}

	public function Path(bool $full = true): string
	{
/*        if(!file_exists($this->location.$this->file)) {
    		throw new InvalidArgumentException("File object says that the path or file (".$this->location.$this->file.") does not exist!");
        }
*/
        return ($full ? $this->location : "").$this->file;
	}

	public function Basename(string $suffix = null): string
	{
		return basename(self::Path(), $suffix);
	}

	public function Exists(): bool
	{
		return file_exists(self::Path());
	}

	public function isHidden(): bool
	{
		return preg_match(self::REGEX_HIDDEN_FILES, self::Path());
	}

	public function isDir(): bool
	{
		return is_dir(self::Path());
	}

	public function isFile(): bool
	{
		return is_file(self::Path());
	}

	public function Temp(string $prefix): string
	{
		return tempnam($this->location, $prefix);
	}

	public function Upload(string $upload_temp_name, string $location, string $rename = null, bool $overwrite = false): bool
	{
		self::setLocation($location);
		if(empty($rename)) $this->file = $rename;
		$this->file = self::Basename();
		if(!$overwrite) self::RenameFileExtender();
		return move_uploaded_file($upload_temp_name, $this->location.$this->file);
	}

	public function isUploaded(): bool
	{
		if(self::isFile()) {
			return is_​uploaded_​file(self::Path());
		}

		return false;
	}

	public function MimeType(): string
	{
		return mime_content_type(self::Path());
	}

	public function Extention(): string
	{
		$position = strpos($this->file, ".");

		return ($position ? (
			$position++ ? explode(".", substr($this->file, $position, strlen($this->file) - $position)) : null
		) : "");
	}

    public function isText(): bool
    {
        return preg_match("/^text\//", self::MimeType());
    }

	public function Lines()
	{
        if(!self::isText()) return false;

		return file(self::Path(), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
	}

	public function Content($uri = false)
	{
        if(!self::isText()) return false;

		if($uri !== false) {
			return urlencode(file_get_contents(self::path()));
		}

		return file_get_contents(self::path());
	}

	public function Size($printUnit = false): string
	{
		$bytes = filesize($this->path());

		if($bytes === null) return "(null)";

		if(!is_numeric($bytes) || $bytes < 0) return null;

		if($printUnit) {
			$n = count($prefixes = array("", "k", "M", "G", "T")); 

			$b = floatval($bytes);
			$i = 0;
			while($b / 1024 > 0.1 && $i < $n - 1) {
				$b /= 1024;
				$i++;
			}
		}

		return ($printUnit ? number_format($b, 2, ",", ".")." ".$prefixes[$i]."B" : $bytes);
	}

	public function Status()
	{
		return stat(self::Path());
	}

	public function Time($option = self::FILE_CHANGE_TIME): TimeStamp
	{
		switch($option) {
			case self::FILE_ACCESS_TIME: return new TimeStamp(fileatime(self::Path()));
			case self::FILE_CHANGE_TIME: return new TimeStamp(filectime(self::Path()));
			case self::FILE_MODIFY_TIME: return new TimeStamp(filemtime(self::Path()));
		}
	}

	public function Inode()
	{
		return fileinode(self::Path());
	}

	public function Type()
	{
		return filetype(self::Path());
	}

	public function isLink(): bool
	{
		return is_link(self::Path());
	}

	public function CreateLink(File $File)
	{
		return link(self::Path(), $File->Path());
	}

	public function CreateSymbolicLink(File $File)
	{
		return symlink(self::Path(), $File->Path());
	}

	public function LinkInfo()
	{
		if(self::isLink()) {
			return linkinfo(self::Path());
		}

		return null;
	}

	public function LinkStatus()
	{
		if(self::isLink()) {
			return lstat(self::Path());
		}

		return null;
	}

	public function ReadLink()
	{
		if(self::isLink()) {
			return readlink(self::Path());
		}

		return null;
	}

	public function ini($parse = self::PARSE_INI_STRING, $process_sections = false, $scanner_mode = INI_SCANNER_NORMAL)
	{
        if(!self::isText()) return false;
        
		switch($parse) {
			case self::PARSE_INI_ARRAY: return parse_ini_file(self::Path(), $process_sections, $scanner_mode);
			case self::PARSE_INI_JSON: return json_decode(parse_ini_file(self::Path(), $process_sections, $scanner_mode));
		}

		return parse_​ini_​string(self::Content(), $process_sections, $scanner_mode);
	}

	public function Download(int $expires = 0)
	{
		header("Content-Description: File Transfer");
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"".self::Basename()."\"");
		header("Expires: ".$expires);
		header("Cache-Control: must-revalidate");
		header("Pragma: public");
		header("Content-Length: ".self::Size());
		readfile(self::Path());
	}

	public function Search(string $pattern)
	{
        if(!self::isText()) return false;

        $match = array();
		if(!preg_match(self::REGEX_PATTERN_STRING, $pattern)) {
			return strstr(self::Content(), $pattern);
		} elseif(preg_match(self::REGEX_PATTERN_MATCH_ALL, $pattern, $match)) {
			return preg_match_all($pattern, self::Content(), $matches) ? $matches : array();
		}

		return preg_match($pattern, self::Content(), $match) ? $match : array();
	}

	public function Insert(string $data, int $position)
	{
        if(!self::isText()) return false;

		return file_put_contents(self::Path(), substr(self::Content(), 0, $position).$data.substr(self::Content(), $position, LOCK_EX));
	}

	public function Append(string $data)
	{
        if(!self::isText()) return false;

		return file_put_contents(self::Path(), $data, FILE_APPEND | LOCK_EX);
	}

	public function Create(string $data)
	{
		if(self::Exists()) return false;

		return file_put_contents(self::Path(), $data, LOCK_EX);
	}

	public function Save(string $data)
	{
		if(!self::Exists()) return false;

		return file_put_contents(self::Path(), $data, LOCK_EX);
	}

	public function getLocation()
	{
		return $this->location;
	}

    public function RenameFileExtender(string $original_filename = null, int $MaxExtLen = 3)
    {
		if(empty($this->file) && empty($original_filename)) return;
		$original_filename = empty($original_filename) ? $this->file : self::Name($original_filename);

		$i = 0;
		$filename = $original_filename;
		$files = scandir($this->location);
		while(in_array($filename, $files, true)) {
			$filename = $original_filename . str_repeat("0", $MaxExtLen - strlen( strval($i) )) . $i;
		}

        $this->$file = $filename;
    }
};
?>