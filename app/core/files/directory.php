<?php

namespace Files;

require_once "files/file.php";

class Directory extends File
{
	const DIR_ONLY = 1;
	const FILES_ONLY = 2;

	private $directory;
	private $rights;

	public function __construct(File $directory, FileRights $rights = null)
	{
		if(!$directory->exists()) {
			self::create($this->rights = $rights);
		}

		if($directory->isDir()) {
			$this->directory = $directory;
			$this->rights = $directory->permissions();
		}
	}

	protected function create($rights = null)
	{
		if(!mkdir($this->location, $rights->isEmpty() ? 0755 : $rights)) {
			die("Unable to create directory ".$this->directory);
		}
	}

	public function treeSize($printUnit = false, &$n = 0)
	{
		$size = filesize($this->directory->path());

		$tree = self::mapFilesFilter();
		$n += count($tree); 

		foreach($tree as $file) {
			if($file->isDir()) {
				$size += self::treeSize(false, $n);
			} else {
				$size += filesize($file->path());
			}
		}

		if($printUnit) {
			$n = count($prefixes = array("", "k", "M", "G", "T")); 

			$b = floatval($size);
			$i = 0;
			while($b / 1024 > 0.1 && $i < $n - 1) {
				$b /= 1024;
				$i++;
			}
		}

		return ($printUnit ? number_format($b, 2, ",", ".")." ".$prefixes[$i]."B" : $size);
	}

	public function mapFilesFilter($filter = 0, $include = array(), $exclude = array(), $show_hidden = false)
	{
		$list = array();
		$filtered_list = array();

		if($this->directory->exists() && $this->directory->isDir()) {
			$list = array_values(array_diff(scandir($this->directory->path()), array("..","."), $exclude));

			if($filter > 0 && $filter <= self::FILES_ONLY && count($list)) {
				foreach($list as $filename) {
					if(isset($filename) && strlen($filename)) {
						$file = new File($this->directory->path(), $filename);
						if(
							$filter === self::DIR_ONLY && $file->isDir() ||
							$filter === self::FILES_ONLY && $file->isFile() ||
							!empty($include) && in_array($filename, $include, true)
						) {
							if($file->isHidden()) {
								if($show_hidden) {
									$filtered_list[] = $file;
								}
							} else {
								$filtered_list[] = $file;
							}
						}
					}
				}
			} else {
				if(count($list)) {
					foreach($list as $filename) {
						$filtered_list[] = new File($this->directory->path(), $filename);
					}
				}
			}
		}

		return $filtered_list;
	}

    private function ScanDirRecursive(string $dir): array
    {
        $array = array_diff(scandir($dir), array(".", ".."));
        foreach($array as $index => $item) {
            if(is_dir($dir."/".$item)) {
                $array[$item] = self::ScanDirRecursive($dir."/".$item);
            }
        }

        return $array;
    }

	public function dirList(): array
	{
		return self::ScanDirRecursive($this->directory);
	}

	public function parent()
	{
		return new Directory(dirname($this->directory));
	}

	public function free()
	{
		return disk_​free_​space($this->directory);
	}

	public function total()
	{
		return disk_​total_​space($this->directory);
	}

	public function info($options)
	{
		if(is_int($options)) {
			return pathinfo($this->directory, $options);
		}

		return false;
	}

/*
opendir() - Open directory handle
readdir() - Read entry from directory handle
closedir() - Close directory handle
*/
	public function wildcardFilter($wildcard)
	{
		if(!is_string($wildcard)) {
			return false;
		}

		return glob($wildcard);
	}

	public function hasFile(File $file)
	{
		return count(self::wildcardFilter($file->path(false)));
	}

	public function hasIndexFile()
	{
		return count(self::wildcardFilter("index.*"));
	}

	public function subdirectories()
	{
		return self::mapFilesFilter(DIR_ONLY);
	}

	public function hasSubdirectories()
	{
		return count(self::subdirectories());
	}
}

?>
