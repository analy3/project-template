<?php

namespace Files;

class FileGroup
{
	private $group;

	public function __construct(string $fileGroup)
	{
		$this->group = $fileGroup;
	}

	public function getFileGroup(): string
	{
		return $this->group;
	}
}
?>
