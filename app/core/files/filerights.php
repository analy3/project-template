<?php

namespace Files;

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "app/core/lib/octal.php";

use Lib\Octal;

define("ALL_FILE_PERM", 0666);
define("NORM_FILE_PERM", 0664);
define("RESTRICTED_FILE_PERM", 0644);
define("NO_FILE_PERM", 0600);

define("ALL_DIR_PERM", 0777);
define("NORM_DIR_PERM", 0775);
define("DEFAULT_DIR_PERM", 0755);
define("RESTRICTED_DIR_PERM", 0755);
define("NO_DIR_PERM", 0700);

//require_once "/var/www/html/include/files/file_group.php";
//require_once "/var/www/html/include/files/file_owner.php";

class FileRights
{
	private $rights;

	public function __construct(Octal $rights, $fileOwner = null, $fileGroup = null)
	{
		if($rights->minOctal(new Octal(0)) && $rights->maxOctal(new Octal(0777))) {
			$this->rights = $rights;
		} else {
			die("File Rights: Argument out of range");
		}
	}

	public function getFileRights()
	{
		return $this->rights;
	}

	public function toString(): string
	{
		return $this->rights->toString();
	}

	public function equalTo(FileRights $rights)
	{
		return $this->rights->getOctal() == $rights->getFileRights()->getOctal();
	}

	public function isExecutable($user)
	{
		return ;
	}

	public function isReadable($user)
	{
		return ;
	}

	public function isWritable($user)
	{
		return ;
	}
}
?>
