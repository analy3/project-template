<?php

namespace Files;

chdir($_SERVER["DOCUMENT_ROOT"]."/project");

require_once "app/core/files/directory.php";
require_once "app/core/lib/timestamp.php";

use Lib\TimeStamp;

class FileManipulation extends Directory
{
	public function __create(string $location, string $file = null, $fileGroup = null, $fileOwner = null, FileRights $fileRights = null)
	{
		$this->file = new File($location, $file, $fileGroup, $fileOwner, $fileRights);
	}

	public function copyTo(Directory $destination)
	{
		if(self::isFile()) {
			return copy(self::path(), $destination->path());
		}

/*
		// Directory version
		if(self::isDir()) {
			create new destination

			// in class Directory
			enlist content (files)
			copy content (files)
		}
*/
	}

	public function delete()
	{
		if(self::isFile() && ulink(self::path())) {
//			return unset(self);
			die;
		}
 
		if(self::isDir()) {
			$files = array_diff(scandir(self::path()), array(".", ".."));
			foreach ($files as $file) {
				self::delete($file->path());
			}
			
			return rmdir($files->path()) or die;// | unset(self);
		}

	}

	public function moveTo(Directory $destination)
	{
		self::copyTo($destination);
		self::delete();
	}

	public function rename($newName)
	{
		if($result = rename(self::path, $newName = self::name($newName))) {
			$this->file = $newName;
		}

		// Directory version
		return $result;
	}

	public function __time(TimeStamp $time = null, TimeStamp $accessTime = null)
	{
		return touch(self::path(), $time, $accessTime);
	}
}
?>
