<?php
if(!isset($index_page)) {
	header("Location: /project");
	exit;
}

define("REGEX_IP_ADDR", "/^(2([0-4]\d|5[0-5])|1?\d{1,2})\.(2([0-4]\d|5[0-5])|1?\d{1,2})\.(2([0-4]\d|5[0-5])|1?\d{1,2})\.(2([0-4]\d|5[0-5])|1?\d{1,2})$/");

if(!isset($relative_dir) || !file_exists($relative_dir)) {
	$relative_dir = "";
}

if($_SESSION["local"] = in_array($_SERVER["SERVER_NAME"], array("localhost", "127.0.0.1"), true)) {
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('output_buffering', 4096);
}
?>