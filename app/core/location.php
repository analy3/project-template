<?php
/*
*
* @project		:	LAVENDAR SITE MAJOR - CONTENT MANAGEMENT SYSTEM
*
* @description	:	Another CMS
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <mike@vandartel-kniknie.nl>
*
*
*
* @date			:	Thursday, 8 April 2021
*
* @launch		:	
*
*
*
* @content		:	Captures the current location (web address) of a page
*
*/

namespace App\Core;

class Location
{
    /* array $path */
    private $path;

    /* string $file */
    private $file;

    /* array $query */
    private $query;

    /* string $location */
    private $location;

    public function __construct(string $href = null)
    {
        if(empty($href)) {
            $this->path = explode("/", trim($_SERVER["SCRIPT_NAME"], "/"), -1);
            $this->file = array_reverse(explode("/", $_SERVER["SCRIPT_NAME"]))[0];
            $this->query = explode("&", $_SERVER["QUERY_STRING"]);
            $this->location = array_reverse($this->path)[0] . ($this->file == "index.php" ? "" : ("/" . $this->file));
        } else {
            $href = explode("?", $href);
            $reversed_path = array_reverse(explode("/", trim($href[0], "/")));
            $isFile = preg_match("/^[a-z0-9\-_]+\.[a-z]{2,4}/", $reversed_path[0]);
            $this->file = $isFile ? $reversed_path[0] : "index.php";
            $lastdir = $reversed_path[$isFile ? 1 : 0];
            if($isFile) unset($reversed_path[0]);
            $this->path = array_reverse($reversed_path);
            $this->query = array_key_exists(1, $href) ? explode("&", $href[1]) : array();
            $this->location = $lastdir . ($this->file == "index.php" ? "" : ("/" . $this->file));
        }
    }

    public function getLocation(): string
    {
        return $this->location;
    }
};
?>