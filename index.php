<?php
/*
*
* @project		:	PROJECT NAME
*
* @description	:	GENERAL PROJECT DESRCIPTION
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	DATE OF COMMENT
*
* @kick-off		:	PROJECT START DATE
*
* @launch		:	PROJECT LAUNCH DATE
*
*
*
* @content		:	Index web page
*
* @remarks		:
*
* @change log	:
*
*/

session_start();

$index_page = true;
$FileDate = new DateTime("@".strval(filemtime("index.php")));
$year = $FileDate->format("Y");

chdir($_SERVER["DOCUMENT_ROOT"] . "/project");

require_once "app/core/server.php";
require_once "app/core/admin.php";
require_once "debugger/watch.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- meta standard -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- meta author -->
<meta name="author" content="analy3.org - M P J van Dartel" />
<!-- meta description of this page -->
<meta name="description" content="" />
<!-- meta web searches -->
<meta name="keywords" content="" />

<?php
if(!empty($sharepicture) && file_exists($sharepicture)) {
	list($width, $height, $type, $attr) = getimagesize($sharepicture);
?>
<meta property="og:image" <?php echo "content=\"".$sharepicture."\""; ?> />
<meta property="og:image:type" <?php echo "content=\"".mime_content_type($sharepicture)."\""; ?> />
<meta property="og:image:width" <?php echo "content=\"".$width."\""; ?> />
<meta property="og:image:height" <?php echo "content=\"".$height."\""; ?> />
<?php
}
?>

<!-- title of the page -->
<title>welcome home</title>

<!-- icon -->
<link rel="icon" type="image/x-icon" href="shared/img/favicon.gif">
<link rel="bookmark icon" type="image/x-icon" href="shared/img/favicon.gif">
<link rel="SHORTCUT ICON" type="image/x-icon" href="shared/img/favicon.gif">

<!-- language -->
<link rel="alternate" hreflang="en" href="http://en.example.com/" />

<!-- style sheets -->
<link rel="stylesheet" type="text/css" href="shared/css/font-awesome.min.css" /> <!-- https://fontawesome.io/icons/ -->
<link rel="stylesheet" type="text/css" href="shared/css/general-watch.css" />

<!-- client based script -->
<script type="text/javascript" src="shared/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="shared/js/angularjs/angular.min.js"></script>
<script type="text/javascript" src="shared/js/general.js"></script>
<script type="text/javascript" src="shared/js/watch.js"></script>

<script type="text/javascript">
</script>

</head>

<body id="mainpage">
</body>

</html>
