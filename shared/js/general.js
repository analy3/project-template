/*
*
* @project		:	PROJECT NAME
*
* @description	:	GENERAL PROJECT DESRCIPTION
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	DATE OF COMMENT
*
* @kick-off		:	PROJECT START DATE
*
* @launch		:	PROJECT LAUNCH DATE
*
*
*
* @content		:	Index web page
*
* @remarks		:
*
* @change log	:
*
*/

function UrlExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}

Number.prototype.Roman = function() {
	if(!(this instanceof Number)) return null;

	var value = parseInt(JSON.stringify(this));

	if(isNaN(value)) return NaN;

	var roman = value < 0 ? "-" : "";
	value = Math.abs(value);

	var lookup = {M: 1000, CM: 900, D: 500, CD: 400, C: 100, XC: 90, L: 50, XL: 40, X: 10, IX: 9, V: 5, IV: 4, I: 1};
	var i;

	for(i in lookup) {
		while(value >= lookup[i]) {
			roman += i;
			value -= lookup[i];
		}
	}

	return roman;
}

Number.prototype.leadingZero = function(n = 1)
{
	if(!(this instanceof Number)) return null;

	var number = parseInt(this);

	if(isNaN(number)) return NaN;

	var leadingZero = "0";

	return leadingZero.repeat(n) + number;
}

Date.prototype.getFullMonth = function(fullLength = 1)
{
	if(!(this instanceof Date)) return null;

	var month;

	if(fullLength) {
		month = this.getMonth();

		switch(month) {
			case 0: month = "January"; break;
			case 1: month = "February"; break;
			case 2: month = "March"; break;
			case 3: month = "April"; break;
			case 4: month = "May"; break;
			case 5: month = "June"; break;
			case 6: month = "July"; break;
			case 7: month = "August"; break;
			case 8: month = "September"; break;
			case 9: month = "October"; break;
			case 10: month = "November"; break;
			case 11: month = "December"; break;
		}
	} else {
		month = this.toString().split(" ");
		month = month[1];
	}

	return month;
}

String.prototype.ucFirst = function()
{
	return this.substr(0, 1).toUpperCase() + this.substr(1);
}

String.prototype.ucWords = function()
{
	if(!this.length) return "";

	var words = this.split(" ");

	words.forEach(function(word, index, arrayOfWords) {
		arrayOfWords[index] = word.ucFirst();
	});

	return words.join(" ");
}

String.prototype.commonDate = function()
{
	var d = this instanceof Object ? new Date(this.toString()) : new Date(this);

	if(isNaN(d)) {
		if(this.toString().match(/^00?(00)?\-00?\-00?$/)) {
			return "Unknown";
		} else if(this.toString().match(/^\d{4}\-00?\-00?$/)) {
			return this.toString().match(/^\d{4}/);
		}
	}

	return (
		d.getFullMonth() + " " +
		d.getDate() + ", " +
		d.getFullYear()
	);
}

String.prototype.htmlspecialcharsDecode = function()
{
	if(!(this instanceof String)) return null;

	var parser = new DOMParser;
	var dom = parser.parseFromString("<!doctype html><body>" + this, "text/html");

	return dom.body.textContent;
}

Object.isObject = function(obj) {
	return (typeof obj === 'object');
}

Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

$(document).ready(function() {
});