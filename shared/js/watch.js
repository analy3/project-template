/*
*
* @project		:	PROJECT NAME
*
* @description	:	GENERAL PROJECT DESRCIPTION
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	DATE OF COMMENT
*
* @kick-off		:	PROJECT START DATE
*
* @launch		:	PROJECT LAUNCH DATE
*
*
*
* @content		:	JQuery "watch" functions. (open/closes WATCH window)
*
*/

function watch()
{
	$(document.body)
		.prepend("<div></div>")
		.children(":first-child")
		.addClass("debug")
		.append("<div></div>")
		.children()
		.append("<div></div>")
		.children()
		.append("<div></div>")
		.append("<div></div>")
		.children()
		.addClass("indent-20")
		.append("<strong></strong>")
		.append("<span></span>")
		.parent()
		.children(":first-child")
		.children(":first-child")
		.text("width: ")
		.next()
		.attr({
			id: "watch-res-width"
		})
		.parent()
		.next()
		.children(":first-child")
		.text("height: ")
		.next()
		.attr({
			id: "watch-res-height"
		})
		.parent()
		.parent()
		.append("<div></div>")
		.children(":last-child")
		.addClass("clear")
		.parent()
		.append("<div><div>")
		.children(":last-child")
		.append("<p></p>")
		.children(":last-child")
		.append("<i></i>")
		.append("<strong></strong>")
		.append("<span></span>")
		.children(":first-child")
		.addClass("fa")
		.addClass("fa-binoculars")
		.next()
		.text("Items to watch: ")
		.next()
		.attr({
			id: "watch-nitems"
		})
		.parent()
		.parent()
		.append("<ol></ol>")
		.children(":last-child")
		.attr({
			id: "watch-list"
		})
		.parent()
		.parent()
		.parent()
		.parent()
		.parent()

		.append("<div></div>")
		.children(":last-child")
		.addClass("debug-window-button")
		.addClass("closed")
		.append("<i></i>")
		.children()
		.addClass("fa")
		.addClass("fa-plus-square-o")
		.attr({
			ariaHidden: "true"
		});
}

function fluedSizer()
{
	$(".debug #watch-res-width").text($(window).width() + "px");
	$(".debug #watch-res-height").text($(window).height() + "px");
}

function watchAPI(JSONfile)
{
	$.getJSON(JSONfile, function(watch) {
	}).fail(function(jqXHR, textStatus, errorThrown) {
		console.log(errorThrown + " Watch: " + jqXHR.responseText);
	}).done(function(watch) {
		$("#watch-nitems")
			.text(watch.length);

		if(!watch.length) return;

		$.each(watch, function(index, watchItem) {
			$.each(watchItem, function(label, variable) {
				$("#watch-list")
					.append("<li></li>")
					.children(":last-child")
					.addClass("watch-row")
					.append("<h5></h5>")
					.children()
					.text(label)
					.parent()
					.append("<div></div>")
					.children(":last-child")
					.html(
						variable
							.replace(/\</g, "&lt;")
							.replace(/\>/g, "&gt;")
							.replace(/\]\=\&gt\;\n/g, "]=\&gt\;<br/>&nbsp;&nbsp;&nbsp;&nbsp;")
							.replace(/\n/g, "<br/>")
							.replace(/\s/g, "&nbsp;")
					);
			});
		});
	});
}

$(document).ready(function() {
	if(location.search.match(/(\?|\&)watch(\=|\&)?/) == null) return;

	watch();

	fluedSizer();

    var dirDepth = (typeof $(document.body).data("dir-depth-relative-to-root") == "number" ? $(document.body).data("dir-depth-relative-to-root") : location.pathname.replace("/project","").split("/").length - 2);

//	console.log(dirDepth);
//	console.log(("../").repeat(dirDepth) + "api/json/watch.json");
	watchAPI(("../").repeat(dirDepth) + "api/json/watch.json");

	$(window).resize(function () {
		fluedSizer();
	});

	$(".debug-window-button").click(function() {
		$(".debug").toggle();
		$(this)
			.toggleClass("closed")
			.children("i")
			.toggleClass("fa-plus-square-o fa-minus-square-o");
	});
});
